import * as React from 'react';
import logo from './logo.svg';
import './App.css';
import './scss/style.scss';
import { Route, Switch, BrowserRouter,Redirect } from 'react-router-dom';

const Login = React.lazy(() => import('./views/pageDefault/login'));

function App() {
  return (
    <BrowserRouter>
      <React.Suspense fallback={"loading.."}>
        <Switch>
          <Route exact path="/" name="Login Page || fiku" render={props => <Login {...props}/>} />
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  );
}

export default App;
